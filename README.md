Eclipse Scout - RT
==================

[Eclipse Scout] [1] is a mature and open framework for modern, service oriented business applications.
It substantially boosts developer productivity and is simple to learn.

This Repository **CI** contains scripts to build Scout on a continuous integration server (Jenkins).


Documentation & Links
---------------------

* [Eclipse Scout Documentation] [2]
* [Eclipse Scout Forum] [3]
* [Eclipse Bugzilla] [4] (Product=Scout; Component=Scout)


Contribution Guidelines
-----------------------

We welcome any kind of contributions (Bug report, documentation, code contribution...).
Please read the [Eclipse Scout Contribution page] [5] to learn more about it.

The contribution process of Eclipse Scout is hosted on tools deployed by the Eclipse Foundation (involing [Bugzilla] [4], Gerrit, Hudson, MediaWiki...).

External tools like the GitHub tracker and pull requests are not supported.


Get in Touch
------------

To get in touch with the Eclipse Scout community, please open a thread in the [Eclipse Scout Forum] [3] or send a mail to [our mailing list] [6]: scout-dev@eclipse.org


License
-------

[Eclipse Public License (EPL) v1.0] [7]


[1]: https://www.eclipse.org/scout/
[2]: https://eclipsescout.github.io/
[3]: https://www.eclipse.org/forums/index.php?t=thread&frm_id=174
[4]: https://bugs.eclipse.org/bugs/
[5]: https://wiki.eclipse.org/Scout/Contribution
[6]: https://accounts.eclipse.org/mailing-list/scout-dev
[7]: https://wiki.eclipse.org/Eclipse_Public_License
